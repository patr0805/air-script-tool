﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor
{
    public static class ExtensionMethods
    {
        public static IEnumerable<T> TakeUntilArrayMatch<T>(this IEnumerable<T> seq, T[] toFind)
        {

            List<T> list = new List<T>(); 
            int toFindCurser = 0;

            foreach (T item in seq)
            {
                if (item.Equals(toFind[toFindCurser]))
                {
                    toFindCurser++;
                }
                else
                {
                    toFindCurser = 0;
                }

                list.Add(item);
                if (toFindCurser == toFind.Length)
                    break;
            }
            list.RemoveRange(list.Count-toFind.Length,toFind.Length);

            return list;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static byte[] StringToByteArrayFastest(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }
    }
}
