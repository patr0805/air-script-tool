﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Air_SCR_editor.SCR;
namespace Air_SCR_editor
{
    static class AirScriptTools
    {
        public static ICommand[] Compile(string path)
        { 
            List<ICommand> entries = new List<ICommand>();
            string[] lines = File.ReadAllLines(path);
            foreach (var line in lines)
            {
                string trimmedLine = line.Trim();
                ICommand command = GetCommand(line);

                if (command != null)
                entries.Add(command);
            }
            PrepareLabels(entries);

            return entries.ToArray();
        }

        private static void PrepareLabels(List<ICommand> entries )
        {
            Dictionary<string, GotoLabel> jumpLabels = new Dictionary<string, GotoLabel>();

            int position = 0;
            foreach (var entry in entries)
            { 

                entry.SetRawPosition(position);
                position += entry.GetCompiledLength();

                if (entry.GetType() == typeof(GotoLabel))
                    jumpLabels.Add(((GotoLabel)entry).GetReferalName(), ((GotoLabel)entry));
            }


            foreach (var entry in entries)
            {
                entry.PointersProcessed(jumpLabels);
            }
            UpdateDialogIndexes(entries);

        }

        private static void UpdateDialogIndexes(List<ICommand> entries)
        {
            short dialogIndex = 0;
            foreach (var entry in entries)
            {
                if (entry.GetType() == typeof (CommandShowDialog))
                {
                    ((CommandShowDialog)entry).DialogIndex = dialogIndex;
                    dialogIndex++;
                }

                if (entry.GetType() == typeof(CommandShowDialog2))
                {
                    ((CommandShowDialog2)entry).DialogIndex = dialogIndex;
                    dialogIndex++;
                } 
            }
        }

        public static ICommand[] Uncompile(string path)
        { 
            List<ICommand> entries = new List<ICommand>();
            byte[] fileBytes = File.ReadAllBytes(path);
            int offset = 0;
             
            while (!IsEndOfFile(fileBytes,offset))
            {
                ICommand command = GetCommand(fileBytes, offset);
                offset += command.GetCompiledLength();
                entries.Add(command); 
            }
            ResolvePointersToLabels(ref entries);

            return entries.ToArray();
        }

        private static void ResolvePointersToLabels(ref List<ICommand> entries)
        { 
            int labelId = 0;
            Dictionary<int,GotoLabel> labels = new Dictionary<int, GotoLabel>();
            foreach (var entry in entries)
            {
                int[] pointers = entry.PointersToProcess();
                if (pointers != null && pointers.Any())
                {
                    Dictionary<int, GotoLabel> labelResult = new Dictionary<int, GotoLabel>();
                    foreach (var pointer in pointers)
                    {
                        if (!labels.ContainsKey(pointer))
                            labels.Add(pointer, new GotoLabel(String.Format("JUMPLABEL:{0}", (labelId++)),pointer-1));
                        if (!labelResult.ContainsKey(pointer))
                        labelResult.Add(pointer,labels[pointer]);
                    }
                    entry.PointersProcessed(labelResult);
                }
            }
            entries.AddRange(labels.Values);
            entries = entries.OrderBy(x => x.GetRawPosition()).ToList(); 
        }

        private static bool IsEndOfFile(byte[] fileBytes, int offset)
        {
            if (offset >= fileBytes.Length)
                return true;
            return false;
        }

        private static ICommand GetCommand(string line)
        {
            ICommand[] cmdCandidates = CommandFactory.GetCommands();
            foreach (var candidate in cmdCandidates)
            {
                ICommand cmd = candidate.TryParse(line);
                if (cmd != null)
                {
                    return cmd;
                }
            }
            return null;
        }
        private static ICommand GetCommand(byte[] fileBytes, int offset)
        {
            ICommand resultingCommand = CommandFactory.GetCommands().FirstOrDefault(x => x.GetId() == fileBytes[offset]);
            if (resultingCommand == null)
                throw new Exception(String.Format("Command [{0}] at offset {1} is not implemented! Import aborted.", fileBytes[offset], offset));

            resultingCommand.LoadEntry(fileBytes,offset);
            return resultingCommand;
        }
    }
}
