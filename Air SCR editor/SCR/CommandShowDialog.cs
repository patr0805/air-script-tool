﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandShowDialog : BaseCommand
    {
        public byte[] UnknownBytes;
        public short DialogIndex;
        public string Text;
        public override int GetId()
        {
            return 0;
        }
        public override int GetCompiledLength()
        {
            const int zeros = 2;
            int actualLength = 1 + UnknownBytes.Length + 2 + Encoding.Unicode.GetBytes(Text).Length + zeros;
            int remainder = actualLength % 4;
            return actualLength + remainder;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(1).ToArray();
            DialogIndex = BitConverter.ToInt16(fileBytes, offset + 2);
            Text = Encoding.Unicode.GetString(fileBytes.Skip(offset + 4).TakeUntilArrayMatch(new byte[] { 0, 0 }).ToArray());
            base.LoadEntry(fileBytes, offset);
        }

        private string WordWrap(string input)
        {
            int lineCounter = 0;
            using (Bitmap _bmp = new Bitmap(1000, 1000))
            {
                StringBuilder finalResult = new StringBuilder();
                using (Graphics g = Graphics.FromImage(_bmp))
                {
                    StringBuilder lineBuilder = new StringBuilder();
                    foreach (string part in input.Replace("\n", " ").Split(' '))
                    {
                        float val = g.MeasureString(lineBuilder.ToString() + " " + part, new Font("MS PGothic", 28.62f)).Width;
                        if (val < 696)
                        {
                            if (lineBuilder.ToString().Length > 0)
                                lineBuilder.Append(" ");
                            lineBuilder.Append(part);
                        }
                        else
                        {
                            lineCounter++;
                            finalResult.Append(lineBuilder.ToString() + "\n");
                            lineBuilder.Clear();
                            lineBuilder.Append(part);
                        }

                    }
                    finalResult.Append(lineBuilder.ToString());

                    if (lineCounter > 2)
                    {
                        Console.WriteLine("More linebreaks than max lines that can be within view of PSP screen:\n{0}", finalResult.ToString());
                    }
                    return finalResult.ToString();
                }
            }
        }

        public override ICommand TryParse(string line)
        {
            if (line.Trim().StartsWith("ShowDialog("))
            {
                string[] splits = line.Trim().Substring("ShowDialog(".Length, line.Length - "ShowDialog(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = String.Join(",", splits.Skip(1).ToArray()).Trim().Substring(1);
                arg1 = arg1.Substring(0, arg1.Length - 1);
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                Text = WordWrap(arg1.Replace(@"\""", @"""").Replace("$n$","\n"));
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("ShowDialog(0x{0},\"{1}\");", ExtensionMethods.ByteArrayToString(UnknownBytes), Text.Replace(@"""", @"\""").Replace("\n", "$n$"));
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(DialogIndex), 0, output, 2, BitConverter.GetBytes(DialogIndex).Length);
            Array.Copy(Encoding.Unicode.GetBytes(Text), 0, output, 4, Encoding.Unicode.GetBytes(Text).Length);
            return output;
        }
    }
}
