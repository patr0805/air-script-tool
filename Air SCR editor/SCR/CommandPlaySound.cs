﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandPlaySound : BaseCommand
    {
        public byte[] UnknownBytes;
        public KeyValuePair<int,int> SoundConfig;

        public override int GetId()
        {
            return 2;
        }
        public override int GetCompiledLength()
        {
            return 12;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(3).ToArray();
            SoundConfig = new KeyValuePair<int, int>(BitConverter.ToInt32(fileBytes, offset + 4), BitConverter.ToInt32(fileBytes, offset + 8));
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("PlaySound("))
            {
                string[] splits = spaceLessLine.Substring("PlaySound(".Length, spaceLessLine.Length - "PlaySound(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                string arg2 = splits[2];
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                SoundConfig = new KeyValuePair<int, int>(Int32.Parse(arg1),Int32.Parse(arg2));
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("PlaySound(0x{0},{1},{2});", ExtensionMethods.ByteArrayToString(UnknownBytes), SoundConfig.Key,SoundConfig.Value);
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(SoundConfig.Key), 0, output, 4, BitConverter.GetBytes(SoundConfig.Key).Length);
            Array.Copy(BitConverter.GetBytes(SoundConfig.Value), 0, output, 8, BitConverter.GetBytes(SoundConfig.Value).Length);
            return output;
        }
    }
}
