﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    interface ICommand
    {
        int GetId();
        int GetCompiledLength();
        void LoadEntry(byte[] fileBytes, int offset);
        int GetRawPosition();
        void SetRawPosition(int newPos);
        int[] PointersToProcess();
        void PointersProcessed(Dictionary<int, GotoLabel> result);
        void PointersProcessed(Dictionary<string, GotoLabel> result);
        byte[] CompileToScr();

        ICommand TryParse(string line);
        string ToString();
    }
}
