﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandRunAirScript : BaseCommand
    {
        public int FileNumber;
        public byte[] UnknownBytes;
        public override int GetId()
        {
            return 51;
        }
        public override int GetCompiledLength()
        {
            return 8;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(3).ToArray();
            FileNumber = BitConverter.ToInt32(fileBytes, offset + 4); 
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("RunAirScript("))
            {
                string[] splits = spaceLessLine.Substring("RunAirScript(".Length, spaceLessLine.Length - "RunAirScript(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                FileNumber = Int32.Parse(arg1); 
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("RunAirScript(0x{0},{1});", ExtensionMethods.ByteArrayToString(UnknownBytes), FileNumber);
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString()); 
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(FileNumber), 0, output, 4, BitConverter.GetBytes(FileNumber).Length); 
            return output;
        }
    }
}
