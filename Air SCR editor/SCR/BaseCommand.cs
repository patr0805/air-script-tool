﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    abstract class BaseCommand : ICommand
    {

        private int _rawPos = 0;
        public abstract int GetId();

        public abstract int GetCompiledLength();

        public virtual void LoadEntry(byte[] fileBytes, int offset)
        {
            _rawPos = offset;
        }

        public void SetRawPosition(int newPos)
        {
            _rawPos = newPos;
        }
        public virtual int GetRawPosition()
        {
            return _rawPos;
        }

        public virtual int[] PointersToProcess()
        {
            return null;
        }

        public virtual void PointersProcessed(Dictionary<int, GotoLabel> result)
        {
         
        }

        public virtual void PointersProcessed(Dictionary<string, GotoLabel> result)
        { 
        }

        public abstract byte[] CompileToScr();

        public abstract ICommand TryParse(string line);


        public override abstract string ToString();
    }
}
