﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandSetDate : BaseCommand
    {
        public byte[] UnknownBytes;
        public string Text;
        public override int GetId()
        {
            return 21;
        }

        public override int GetCompiledLength()
        {
            const int zeros = 2;
            int actualLength = 1 + UnknownBytes.Length + Encoding.Unicode.GetBytes(Text).Length + zeros;
            int remainder = actualLength % 4;
            return actualLength + remainder;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(3).ToArray();
            Text = Encoding.Unicode.GetString(fileBytes.Skip(offset + 4).TakeUntilArrayMatch(new byte[] { 0, 0 }).ToArray());
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        { 
            if (line.Trim().StartsWith("SetDateText("))
            {
                string[] splits = line.Trim().Substring("SetDateText(".Length, line.Length - "SetDateText(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = String.Join(",",splits.Skip(1).ToArray()).Trim().Substring(1);
                arg1 = arg1.Substring(0, arg1.Length - 1);
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                Text = arg1.Replace(@"\""", @"""").Replace("$n$", "\n");
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("SetDateText(0x{0},\"{1}\");", ExtensionMethods.ByteArrayToString(UnknownBytes), Text.Replace(@"""", @"\""").Replace("\n", "$n$"));
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(Encoding.Unicode.GetBytes(Text), 0, output, 4, Encoding.Unicode.GetBytes(Text).Length);
            return output;
        }
    }
}
