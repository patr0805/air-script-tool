﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class GotoLabel : ICommand
    {
        public string LabelValue;
        private int _rawPosition;

        public GotoLabel(string labelValue, int rawPosition)
        {
            LabelValue = labelValue;
            _rawPosition = rawPosition;
        }

        public int GetId()
        {
            return -1;
        }

        public int GetCompiledLength()
        {
            return 0;
        }

        public void LoadEntry(byte[] fileBytes, int offset)
        {
            throw new InvalidOperationException();
        }

        public int GetRawPosition()
        {
            return _rawPosition;
        }

        public void SetRawPosition(int newPos)
        {
            _rawPosition = newPos;
        }

        public int[] PointersToProcess()
        {
            return null;
        }

        public void PointersProcessed(Dictionary<int, GotoLabel> result)
        { 
        }

        public void PointersProcessed(Dictionary<string, GotoLabel> result)
        { 
        }

        public byte[] CompileToScr()
        {
            return new byte[0];
        }

        public ICommand TryParse(string line)
        {
            if (line.StartsWith("[") && line.EndsWith("]"))
            { 
                LabelValue = line.Substring(1, line.Length - 2);
                _rawPosition = -1;
                return this;
            }
            return null;

        }

        public string GetReferalName()
        {
            return LabelValue;
        }

        public override string ToString()
        {
            return string.Format("[{0}]",GetReferalName());
        }
    }
}
