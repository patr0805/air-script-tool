﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandPlayVideo : BaseCommand
    {
        public byte[] UnknownBytes;
        public int VideoId;

        public override int GetId()
        {
            return 50;
        }
        public override int GetCompiledLength()
        {
            return 8;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(3).ToArray();
            VideoId = BitConverter.ToInt32(fileBytes, offset + 4);
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("PlayVideo("))
            {
                string[] splits = spaceLessLine.Substring("PlayVideo(".Length, spaceLessLine.Length - "PlayVideo(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1]; 
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                VideoId =  Int32.Parse(arg1);
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("PlayVideo(0x{0},{1});", ExtensionMethods.ByteArrayToString(UnknownBytes),VideoId);
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(VideoId), 0, output, 4, BitConverter.GetBytes(VideoId).Length);
            return output;
        }
    }
}
