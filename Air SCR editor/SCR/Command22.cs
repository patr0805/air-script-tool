﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class Command22 : BaseCommand
    {
        private byte[] bytes;

        public override int GetId()
        {
            return 22;
        }

        public override int GetCompiledLength()
        {
            return 12;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            bytes = fileBytes.Skip(offset+1).Take(GetCompiledLength()-1).ToArray();
            base.LoadEntry(fileBytes,offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("CMDRAW(0x16"))
            {
                string[] splits = spaceLessLine.Substring("CMDRAW(0x16,".Length, spaceLessLine.Length - "CMDRAW(0x16,".Length -2).Split(',');
                string hex = splits[0];
                bytes = ExtensionMethods.StringToByteArrayFastest(hex.Replace("0x", ""));
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("CMDRAW(0x16,0x{0});", ExtensionMethods.ByteArrayToString(bytes));
        }


        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(bytes,0,output,1,bytes.Length);
            return output;
        }
    }
}
