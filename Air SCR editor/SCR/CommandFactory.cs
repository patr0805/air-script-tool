﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    static class CommandFactory
    {
        public static ICommand[] GetCommands()
        {
            return new ICommand[] { new Command22(), new Command24(), new CommandGoto(), new CommandConditionalGoto(), new CommandShakeScreen(), new CommandChangeBackgroundImage(), new CommandChangeBackgroundMusic(), new CommandEndOfFile(), new CommandPlaySound(), new CommandPlayVideo(), new CommandPlayVoice(), new CommandRunAirScript(), new CommandSetDate(), new CommandShowDialog(), new CommandShowDialog2(), new CommandShowQuestionDialog(), new GotoLabel("", 0), new CommandDelay() , new Command52()};
        }
    }
}
