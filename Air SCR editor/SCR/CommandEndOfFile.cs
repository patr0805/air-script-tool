﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandEndOfFile : BaseCommand
    {
        public override int GetId()
        {
            return 254;
        }

        public override int GetCompiledLength()
        {
            return 1;
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("EndOfFile("))
            {
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("EndOfFile();");
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString()); 
            return output;
        }
    }
}
