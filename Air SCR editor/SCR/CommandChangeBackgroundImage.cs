﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandChangeBackgroundImage : BaseCommand
    {
        public byte[] UnknownBytesBeforeImageId;
        public byte[] UnknownBytesAfterImageId;
        public int ImageId;

        public override int GetId()
        {
            return 10;
        }
        public override int GetCompiledLength()
        {
            return 12;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytesBeforeImageId = fileBytes.Skip(offset + 1).Take(3).ToArray();
            ImageId = BitConverter.ToInt32(fileBytes, offset + 4);
            UnknownBytesAfterImageId = fileBytes.Skip(offset + 8).Take(2).ToArray();
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("ChangeBackgroundImage("))
            {
                string[] splits = spaceLessLine.Substring("ChangeBackgroundImage(".Length, spaceLessLine.Length - "ChangeBackgroundImage(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                string arg2 = splits[2];
                UnknownBytesBeforeImageId = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                ImageId = Int32.Parse(arg1);
                UnknownBytesAfterImageId = ExtensionMethods.StringToByteArrayFastest(arg2.Replace("0x", ""));
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("ChangeBackgroundImage(0x{0},{1},0x{2});", ExtensionMethods.ByteArrayToString(UnknownBytesBeforeImageId), ImageId, ExtensionMethods.ByteArrayToString(UnknownBytesAfterImageId));
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytesBeforeImageId, 0, output, 1, UnknownBytesBeforeImageId.Length);
            Array.Copy(BitConverter.GetBytes(ImageId), 0, output, 4, BitConverter.GetBytes(ImageId).Length);
            Array.Copy(UnknownBytesAfterImageId, 0, output, 8, UnknownBytesAfterImageId.Length);
            return output;
        }
    }
}
