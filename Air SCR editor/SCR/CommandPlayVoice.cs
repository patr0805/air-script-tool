﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandPlayVoice : BaseCommand
    {
        public byte CharacterIndex;
        public byte[] UnknownBytes;
        public KeyValuePair<int, int> SoundConfig;
        public override int GetId()
        {
            return 1;
        }
        public override int GetCompiledLength()
        {
            return 12;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            CharacterIndex = fileBytes[offset + 1];
            UnknownBytes = fileBytes.Skip(offset + 2).Take(2).ToArray();
            SoundConfig = new KeyValuePair<int, int>(BitConverter.ToInt32(fileBytes, offset + 4), BitConverter.ToInt32(fileBytes, offset + 8));
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("PlayVoice("))
            {
                string[] splits = spaceLessLine.Substring("PlayVoice(".Length, spaceLessLine.Length - "PlayVoice(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                string arg2 = splits[2];
                string arg3 = splits[3];
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                CharacterIndex = byte.Parse(arg1);
                SoundConfig = new KeyValuePair<int, int>(Int32.Parse(arg2), Int32.Parse(arg3));
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("PlayVoice(0x{0},{1},{2},{3});", ExtensionMethods.ByteArrayToString(UnknownBytes), CharacterIndex,SoundConfig.Key,SoundConfig.Value);
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            output[1] = CharacterIndex;
            Array.Copy(UnknownBytes, 0, output, 2, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(SoundConfig.Key), 0, output, 4, BitConverter.GetBytes(SoundConfig.Key).Length);
            Array.Copy(BitConverter.GetBytes(SoundConfig.Value), 0, output, 8, BitConverter.GetBytes(SoundConfig.Value).Length);
            return output;
        }
    }
}
