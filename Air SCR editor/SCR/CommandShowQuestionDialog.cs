﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandShowQuestionDialog : BaseCommand
    { 
        public byte[] UnknownBytes;
        private int[] pointers;
        private string[] lines;
        private GotoLabel[] labels;

        private string[] parseLabelNames;

        public override int GetId()
        {
            return 25;
        }

        public override int GetCompiledLength()
        {
            const int zeros = 2;
            var q = Encoding.Unicode.GetBytes(String.Join("\n", lines.Where(arg => !string.IsNullOrWhiteSpace(arg)).ToArray()));
            int actualLength = 1 + UnknownBytes.Length + 12 + q.Length + zeros;
            int remainder = actualLength % 4;
            return actualLength + remainder;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(3).ToArray();
            var pointerOne = BitConverter.ToInt32(fileBytes, offset + 4);
            var pointerTwo = BitConverter.ToInt32(fileBytes, offset + 8);
            var pointerThree = BitConverter.ToInt32(fileBytes, offset + 12);
            pointers = new[] {pointerOne, pointerTwo, pointerThree};
            lines = Encoding.Unicode.GetString(fileBytes.Skip(offset + 16).TakeUntilArrayMatch(new byte[] { 0, 0 }).ToArray()).Split('\n');
            labels = new GotoLabel[3];
            base.LoadEntry(fileBytes,offset);
        }

        public override int[] PointersToProcess()
        {
            return pointers;
        }

        public override void PointersProcessed(Dictionary<int, GotoLabel> result)
        {
            for (int i = 0; i < pointers.Length; i++)
            {
                labels[i] = result[pointers[i]];
            }
        }

        public override void PointersProcessed(Dictionary<string, GotoLabel> result)
        {
            for (int i = 0; i < pointers.Length; i++)
            {
                labels[i] = result[parseLabelNames[i]];
                pointers[i] = result[parseLabelNames[i]].GetRawPosition();
            }
        }

        public override ICommand TryParse(string line)
        {
            if (line.Trim().StartsWith("ShowQuestion("))
            {
                lines = new string[3];
                labels = new GotoLabel[3];
                parseLabelNames = new string[3];
                pointers = new int[3];
                string[] splits = line.Trim().Substring("ShowQuestion(".Length, line.Length - "ShowQuestion(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                string arg2 = splits[2];
                string arg3 = splits[3];

                string cmd = String.Join(",", splits.Skip(4).ToArray()).Replace( @"\""",@"<!QUOTE!>");
                MatchCollection matches = Regex.Matches(cmd, "\"[^\"]*\"");  
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                parseLabelNames[0] = arg1;
                parseLabelNames[1] = arg2;
                parseLabelNames[2] = arg3;
                lines[0] = matches[0].Value.Substring(1, matches[0].Value.Length - 2).Replace("<!QUOTE!>", @"""").Replace("$n$", "\n");
                lines[1] = matches[1].Value.Substring(1, matches[1].Value.Length - 2).Replace("<!QUOTE!>", @"""").Replace("$n$", "\n");
                lines[2] = matches[2].Value.Substring(1, matches[2].Value.Length - 2).Replace("<!QUOTE!>", @"""").Replace("$n$", "\n");
                return this;
            }
            return null;
        }

        public override string ToString()
        {
            return string.Format("ShowQuestion(0x{0},{1},{2},{3},\"{4}\",\"{5}\",\"{6}\");", ExtensionMethods.ByteArrayToString(UnknownBytes), labels[0].GetReferalName(), labels[1].GetReferalName(), labels[2].GetReferalName(), lines.Length > 0 ? lines[0].Replace(@"""", @"\""").Replace("\n", "$n$") : "", lines.Length > 1 ? lines[1].Replace(@"""", @"\""").Replace("\n", "$n$") : "", lines.Length > 2 ? lines[2].Replace(@"""", @"\""").Replace("\n", "$n$") : "");
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(pointers[0]), 0, output, 4, BitConverter.GetBytes(pointers[0]).Length);
            Array.Copy(BitConverter.GetBytes(pointers[1]), 0, output, 8, BitConverter.GetBytes(pointers[1]).Length);
            Array.Copy(BitConverter.GetBytes(pointers[2]), 0, output, 12, BitConverter.GetBytes(pointers[2]).Length);
            var q = Encoding.Unicode.GetBytes(String.Join("\n", lines.Where(arg => !string.IsNullOrWhiteSpace(arg)).ToArray()));
            Array.Copy(q, 0, output, 16, q.Length);
            return output;
        }
    }
}
