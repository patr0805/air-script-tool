﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Air_SCR_editor.SCR
{
    class CommandConditionalGoto : BaseCommand
    {
        private byte[] UnknownBytes;
        private int pointer;
        private GotoLabel label;
        private string parseLabelName;

        public override int GetId()
        {
            return 27;
        }
        public override int GetCompiledLength()
        {
            return 16;
        }

        public override void LoadEntry(byte[] fileBytes, int offset)
        {
            UnknownBytes = fileBytes.Skip(offset + 1).Take(11).ToArray();
            pointer = BitConverter.ToInt32(fileBytes, offset + 12);
            base.LoadEntry(fileBytes, offset);
        }

        public override ICommand TryParse(string line)
        {
            string spaceLessLine = line.Replace(" ", "");
            if (spaceLessLine.StartsWith("GotoConditional("))
            {
                string[] splits = spaceLessLine.Substring("GotoConditional(".Length, spaceLessLine.Length - "GotoConditional(".Length - 2).Split(',');
                string arg0 = splits[0];
                string arg1 = splits[1];
                UnknownBytes = ExtensionMethods.StringToByteArrayFastest(arg0.Replace("0x", ""));
                parseLabelName = arg1;
                return this;
            }
            return null;
        }

        public override void PointersProcessed(Dictionary<int, GotoLabel> result)
        {
            label = result[pointer];
        }

        public override int[] PointersToProcess()
        {
            return new[] { pointer };
        }

        public override void PointersProcessed(Dictionary<string, GotoLabel> result)
        {
            label = result[parseLabelName];
            pointer = result[parseLabelName].GetRawPosition();
        }

        public override string ToString()
        {
            return string.Format("GotoConditional(0x{0},{1});", ExtensionMethods.ByteArrayToString(UnknownBytes), label.GetReferalName());
        }

        public override byte[] CompileToScr()
        {
            byte[] output = new byte[GetCompiledLength()];
            output[0] = byte.Parse(GetId().ToString());
            Array.Copy(UnknownBytes, 0, output, 1, UnknownBytes.Length);
            Array.Copy(BitConverter.GetBytes(pointer), 0, output, 12, BitConverter.GetBytes(pointer).Length);

            return output;
        }
    }
}
