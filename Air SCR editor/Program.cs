﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Air_SCR_editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine(@"--------------------------------------------");
            Console.WriteLine(@"Air SCR ScriptFile Tool by Patrick Bjoerkman");
            Console.WriteLine(@"--------------------------------------------");

            if (args.Length != 3)
                WriteUsageLog();
            else
            {
                string inputFilePath = args[1];
                string outputFilePath = args[2];

                //Validate inputfile
                if (!CanFileBeOpened(inputFilePath))
                {
                    Console.WriteLine("Input file could not be opened!");
                    WriteUsageLog();
                    return;
                }   
                string operation = args[0];
                switch (operation)
                {
                    case "-u":
                        {
                            var entries = AirScriptTools.Uncompile(inputFilePath);
                            StringBuilder stringBuilder = new StringBuilder();
                            foreach (var cmd in entries)
                            {
                                stringBuilder.AppendLine(cmd.ToString());
                            }
                            File.WriteAllText(outputFilePath, stringBuilder.ToString(), Encoding.Unicode);
                            break;
                    } 
                    case "-c":
                    {
                        var entries = AirScriptTools.Compile(inputFilePath);
                        List<byte> output = new List<byte>();
                        foreach (var cmd in entries)
                        {
                            output.AddRange(cmd.CompileToScr());
                        }
                        File.WriteAllBytes(outputFilePath, output.ToArray());
                        break;
                    }
                    case "-p":
                    {
                        var entries = SeenutfProcessor.GetEntries(outputFilePath);
                        var correctedLines = SeenutfProcessor.Replace(entries,inputFilePath);
                        File.WriteAllLines(inputFilePath, correctedLines,Encoding.Unicode);
                        break;
                        }
                    default:
                        WriteUsageLog();
                        return;
                }
            }
        }

        static bool CanFileBeOpened(string path)
        {
            try {
                using (FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    return true; // <- File has been opened
                }
            }
                  catch (IOException) {
                    return false; // <- File failed to open
                  }
        }

        static void WriteUsageLog()
        {
            Console.WriteLine(@"Invalid usage! Here is the usage specification:");
            Console.WriteLine(@"AirSCRTool.exe -u file.scr file.airscript (Uncompile scr into airscript)");
            Console.WriteLine(@"AirSCRTool.exe -c file.airscript file.scr (Compile airscript into scr)");
        }
    }
}
