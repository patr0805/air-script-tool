﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Air_SCR_editor
{
    internal static class SeenutfProcessor
    {
        public static List<KeyValuePair<string,string>> GetEntries(string path)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            string[] lines = File.ReadAllLines(path); 
            for (int counter = 0; counter < lines.Length;counter++ )
            {
                string line = lines[counter];

                if (counter > 0 && line.TrimStart().StartsWith(@"//"))
                {
                    if (lines[counter].Contains("Lily:"))
                        continue;
                    if (counter + 1 == lines.Length || lines[counter + 1].TrimStart().StartsWith(@"//"))
                    {
                        string candidate = lines[counter];
                        if (candidate.Contains("Lily:"))
                            continue;
                        string candidate2 = lines[counter + 1]; 
                        if (candidate == "mfwrogimgrkg")
                        continue;
                    }
                    string value = line.Substring(line.IndexOf(@"//") + @"//".Length).TrimStart();
                    value = value.Replace(".", "。").Replace("Yukito", "往人").Replace(@"{Granny}", @"{婆さん}").Replace(@"{Kid}", @"{子供}").Replace(@"{Female student}", @"{女子学生}").Replace(@"{Students}", @"{学生たち}").Replace(@"{Yukito}", @"{往人}").Replace("{Lady}", "{おばさん}").Replace("{Girl}", "{少女}").Replace("{Misuzu}", "{観鈴}").Replace("{観鈴の母}", "{Mom}").Replace("{TV}", "{テレビ}").Replace("{Haruko}", "{晴子}").Replace("{Voice}", "{声}");
                    if (value.Contains(@"\shake"))
                    {
                        value = value.Substring(0, value.IndexOf(@"\shake"));

                    }
                    if (value.StartsWith(@"\{"))
                    {
                        value = value.Substring(value.IndexOf("{") + 1);
                        value = value.Substring(0, value.IndexOf("}")) + value.Substring(value.IndexOf("}") + 1);
                    }
                    int counter2 = 0;
                    for (int i = counter; i > 0; i--)
                    {
                        if (lines[i].StartsWith("<"))
                        {
                            break;
                        }
                        counter2++;
                    }
                    string upperEntry = lines[counter - counter2];
                    if (upperEntry.StartsWith("<"))
                    {
                        upperEntry = upperEntry.Substring(@"<0017> ".Length);
                        if (upperEntry.StartsWith(@"\{"))
                        {
                            upperEntry = upperEntry.Substring(2);
                            upperEntry = upperEntry.Substring(0, upperEntry.IndexOf("}")) + ": " + upperEntry.Substring(upperEntry.IndexOf("}") + 1);
                        }
                        if (upperEntry.Contains(@"\shake"))
                        {
                            upperEntry = upperEntry.Substring(0, upperEntry.IndexOf(@"\shake"));

                        }

                        result.Add(new KeyValuePair<string, string>(value.Replace("!", "！").Replace("(", "（").Replace(")", "）").Replace("　", " "), FixLength(upperEntry.Replace(@"""", @"\"""))));
                        if (upperEntry.Contains("Yukito-san, four eggs"))
                            Console.WriteLine("j");
                        for (int i = counter; i < lines.Length; i++)
                        {
                            if (lines[i].StartsWith("<"))
                            {
                                break;
                            }
                            counter++;
                        }
                        counter--;
                    }
                }
            }
            return result;

        }

        public static string[] Replace(List<KeyValuePair<string, string>> entries, string path)
        {
            string[] lines = File.ReadAllLines(path); 
            int subCounter = 0;
            for (int counter = 0; counter < lines.Length;counter++ )
            {
                string line = lines[counter];
                if (subCounter >= entries.Count)
                {
                    
                if (line.StartsWith("ShowDialog") || line.StartsWith("SetDateText("))
                {
                    bool fo = false;
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    string newContent = content.Replace("〜", "～").Replace("　", " ");
                    for (int i = subCounter; i < entries.Count; i++)
                    {
                        if (entries[i].Key.Equals(newContent))
                        {
                            string t = lines[counter];
                            KeyValuePair<string, string> t2 = entries[i];
                            subCounter = i;
                            fo = true;
                            break;
                        }
                    }
                    if (!fo)
                        for (int i = 0; i < entries.Count; i++)
                        {
                            if (entries[i].Key.Equals(newContent))
                            {
                                string t = lines[counter];
                                KeyValuePair<string, string> t2 = entries[i];
                                subCounter = i;
                                fo = true;
                                break;
                            }
                        } 
                if (!fo)
                    break;
                }
                }
                if (line.StartsWith("ShowDialog") || line.StartsWith("SetDateText("))
                {
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    string newContent = content.Replace("〜", "～").Replace("　", " ");

                    var taav = entries[subCounter].Key.Substring(entries[subCounter].Key.IndexOf(">") + 1);
                    taav = taav.Trim().StartsWith(@"\{") ? taav.Substring(taav.IndexOf("{")+1,taav.Substring(taav.IndexOf("{")+1).IndexOf("}")) + taav.Substring(taav.IndexOf("}")+1) : taav;

                    if (newContent.Trim() == taav.Trim())
                    {
                        lines[counter] = line.Replace(content, entries[subCounter].Value);
                    }
                    else
                    {
                        if (WasMatch(lines, counter - 1, entries, subCounter) && IsMatch(lines, counter + 1, entries, subCounter))
                        {
                            string t = lines[counter];
                            KeyValuePair<string, string> t2 = entries[subCounter];
                            lines[counter] = line.Replace(content, entries[subCounter].Value);
                        }
                        else
                        {
                            bool fo = false;
                            for (int i = subCounter; i < entries.Count; i++)
                            { 
                                if (entries[i].Key.Substring(entries[i].Key.IndexOf(">")+1).Trim().Equals(newContent.Trim()))
                                {
                                    string t = lines[counter];
                                    KeyValuePair<string, string> t2 = entries[i];
                                    subCounter = i;
                                    fo = true;
                                    break;
                                }
                            }
                                if (!fo)
                                for (int i = 0; i < entries.Count; i++)
                                {
                                    if (entries[i].Key.Equals(newContent))
                                    {
                                        string t = lines[counter];
                                        KeyValuePair<string, string> t2 = entries[i];
                                        subCounter = i;
                                        fo = true;
                                        break;
                                    }
                                }

                            string ta = lines[counter];
                            KeyValuePair<string, string> t2a = entries[subCounter];
                            Console.WriteLine("Out of sync");
                            if (fo)
                            {
                                counter--;
                                continue;
                            } 
                            if (!fo)
                            {
                                string nLine = newContent;
                                string nLinea = entries[subCounter].Key;
                                string nval = line.Replace(content, entries[subCounter].Value);
                                lines[counter] = nval ; 
                            } 
                        }

                    }

                    subCounter++;
                }
                if (line.StartsWith("ShowQuestion("))
                {
                    string contentpre = line.Substring(line.IndexOf(@""""));
                    contentpre = contentpre.Substring(0, contentpre.IndexOf(");"));
                    string[] contents = contentpre.Split(',');
                    string c0 = contents[0].Substring(1, contents[0].Length - 2);
                    string c1 = contents[1].Substring(1, contents[1].Length - 2);
                    string c2 = contents[2].Substring(1, contents[2].Length - 2);
                    if (contents.Length > 3)
                    {
                        Console.WriteLine("OUT OF SYNC!");

                    }

                    int count = 0;

                    string[] contentsa  =new string[] { c0, c1, c2 };

                    for (int countera = 0; countera < contents.Length; countera++) 
                    {
                        string content = contentsa[countera];
                        if (string.IsNullOrEmpty(content))
                            continue;
                        string newContent = content.Replace("〜", "～").Replace("　", " ");
                        if (newContent == entries[subCounter + count].Key)
                        {
                            lines[counter] = lines[counter].Replace(content, entries[subCounter + count].Value);
                        }
                        else
                        {
                            if (WasMatch(lines, counter - 1, entries, subCounter) && IsMatch(lines, counter + 1, entries, subCounter))
                            {
                                lines[counter] = line.Replace(content, entries[subCounter + count].Value);
                            }
                            else
                            {
                                bool fo = false;
                                for (int i = subCounter; i < entries.Count; i++)
                                {
                                    if (entries[i].Key.Equals(newContent))
                                    {
                                        string t = lines[counter];
                                        KeyValuePair<string, string> t2 = entries[i];
                                        subCounter = i;
                                        fo = true;
                                        break;
                                    }
                                }
                                if (!fo)
                                    for (int i = 0; i < entries.Count; i++)
                                    {
                                        if (entries[i].Key.Equals(newContent))
                                        {
                                            string t = lines[counter];
                                            KeyValuePair<string, string> t2 = entries[i];
                                            subCounter = i;
                                            fo = true;
                                            break;
                                        }
                                    }

                                string ta = lines[counter];
                                KeyValuePair<string, string> t2a = entries[subCounter];
                                Console.WriteLine("Out of sync");
                                if (fo)
                                {
                                    countera--;
                                    continue;
                                }
                                if (!fo)
                                {
                                    string nLine = newContent;
                                    string nLinea = entries[subCounter].Key;
                                    string nval = line.Replace(content, entries[subCounter].Value);
                                    lines[counter] = nval;
                                }
                            }

                        }
                        count++;
                    }
                    subCounter += (string.IsNullOrEmpty(c0) ? 0 : 1) + (string.IsNullOrEmpty(c1) ? 0 : 1) + (string.IsNullOrEmpty(c2) ? 0 : 1);
                }

            }
            return lines;

        }
        private static bool WasMatch(string[] lines,int counter, List<KeyValuePair<string, string>> entry, int subCounter)
        {
            for (int i = counter; i > -1; i--)
            {
                string line = lines[i];
                if (line.StartsWith("SetDateText("))
                {
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    return content.Equals(entry[subCounter - 1].Value);
                }
                if (line.StartsWith("ShowDialog"))
                {
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    return content.Equals(entry[subCounter - 1].Value);
                }
                if (line.StartsWith("ShowQuestion("))
                {
                    KeyValuePair<string, string>[] cases = new KeyValuePair<string, string>[] { entry[subCounter - 3], entry[subCounter - 2], entry[subCounter - 1] };

                    string contentpre = line.Substring(line.IndexOf(@""""));
                    contentpre = contentpre.Substring(0, contentpre.IndexOf(");"));
                    string[] contents = contentpre.Split(',');
                    string c0 = contents[0].Substring(1, contents[0].Length - 2);
                    string c1 = contents[1].Substring(1, contents[1].Length - 2);
                    string c2 = contents[2].Substring(1, contents[2].Length - 2);

                    return (new string[] { c0, c1, c2 }).All(x => x.Equals("") || cases.Any(y => y.Value == x));
                }
            } 
            throw new Exception("MUST NOT BE REACHED! MALFORMED INPUT!");
        }
        private static bool IsMatch(string[] lines, int counter, List<KeyValuePair<string, string>> entry, int subCounter)
        {

            for (int i = counter; i < lines.Length; i++)
            {
                string line = lines[i];

                if (line.StartsWith("SetDateText("))
                {
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    string newContent = content.Replace("〜", "～").Replace("　", " ");
                    return newContent.Equals(entry[subCounter + 1].Key);
                }
                if (line.StartsWith("ShowDialog"))
                {
                    string content = line.Substring(line.IndexOf(@"""") + 1);
                    content = content.Substring(0, content.IndexOf(@""")"));
                    string newContent = content.Replace("〜", "～").Replace("　", " ");
                    return newContent.Equals(entry[subCounter + 1].Key);
                }
                if (line.StartsWith("ShowQuestion("))
                {
                    KeyValuePair<string, string>[] cases = new KeyValuePair<string, string>[] { entry[subCounter - 3], entry[subCounter - 2], entry[subCounter - 1] };

                    string contentpre = line.Substring(line.IndexOf(@""""));
                    contentpre = contentpre.Substring(0, contentpre.IndexOf(");"));
                    string[] contents = contentpre.Split(',');
                    string c0 = contents[0].Substring(1, contents[0].Length - 2).Replace("〜", "～").Replace("　", " ");
                    string c1 = contents[1].Substring(1, contents[1].Length - 2).Replace("〜", "～").Replace("　", " ");
                    string c2 = contents[2].Substring(1, contents[2].Length - 2).Replace("〜", "～").Replace("　", " "); 

                    return (new string[] { c0, c1, c2 }).All(x => x.Equals("") || cases.Any(y => y.Key == x));
                }
            }
            throw new Exception("MUST NOT BE REACHED! MALFORMED INPUT!");
        }
        private static string FixLength(string input)
        {
            string[] split = input.Split(' ');
            StringBuilder builder = new StringBuilder();
            string cur = "";
            using (var _bmp = new Bitmap(10, 10))
            {
                using (Graphics g = Graphics.FromImage(_bmp))
                {
                    foreach (var word in split)
                    {
                        if (g.MeasureString(cur + word, new Font("MS PGothic", 28.62f)).Width > 700)
                        {
                            builder.Append(cur);
                            builder.Append("$n$");
                            cur = word;
                        }
                        else
                        {
                            cur +=" "+ word;
                            cur = cur.TrimStart();
                        }
                    }
                    builder.Append(cur);
                }
            }
            return builder.ToString();

        }

    }
}
